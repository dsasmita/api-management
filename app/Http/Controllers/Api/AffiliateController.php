<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;

class AffiliateController extends ApiGuardController
{
	/*
    protected $apiMethods = [
       'pub_token' => [
           'keyAuthentication' => false
       ],
       'ref_token' => [
           'keyAuthentication' => false
       ],
    ];
    */

    public function form(Request $request){
      $key = $request->input('secret_key');
      $apiKey = \App\Model\ApiKeys::where('key',$key)->first();
      if(!$apiKey){
        return response()->json(['error'=>[
                              'method'      => 'form',
                              'status'      => 'NOT-OK',
                              'message'     => 'unknow API Key',
                              'code'        => "GEN-UNAUTHORIZED",
                              'http_code'   => 401
                            ]],401);
      }

      $affiliate = \App\Model\Affiliate::where('id',$apiKey->user_id)->first();
      if(!$affiliate){
        return response()->json(['error'=>[
                              'method'      => 'form',
                              'status'      => 'NOT-OK',
                              'message'     => 'API Key not assosiate with affiliate',
                              'code'        => "GEN-UNAUTHORIZED",
                              'http_code'   => 401
                            ]],401);
      }

      if($affiliate->status != 'active'){
        return response()->json(['error'=>[
                              'method'      => 'form',
                              'status'      => 'NOT-OK',
                              'message'     => 'status affiliate not active',
                              'code'        => "GEN-UNAUTHORIZED",
                              'http_code'   => 401
                            ]],401);
      }

      return response()->json(['success'=>[
                              'method'      => 'form',
                              'status'      => 'OK',
                              'affiliate'   => $affiliate,
                              'message'     => 'well done dude',
                              'code'        => "AUTHORIZED",
                              'http_code'   => 201
                            ]],201);
    }

    public function downloadPdf(Request $request){
      $key = $request->input('secret_key');
      $apiKey = \App\Model\ApiKeys::where('key',$key)->first();
      if(!$apiKey){
        return response()->json(['error'=>[
                              'method'      => 'download',
                              'status'      => 'NOT-OK',
                              'message'     => 'unknow API Key',
                              'code'        => "GEN-UNAUTHORIZED",
                              'http_code'   => 401
                            ]],401);
      }

      $affiliate = \App\Model\Affiliate::where('id',$apiKey->user_id)->first();
      if(!$affiliate){
        return response()->json(['error'=>[
                              'method'      => 'download',
                              'status'      => 'NOT-OK',
                              'message'     => 'API Key not assosiate with affiliate',
                              'code'        => "GEN-UNAUTHORIZED",
                              'http_code'   => 401
                            ]],401);
      }

      if($affiliate->status != 'active'){
        return response()->json(['error'=>[
                              'method'      => 'download',
                              'status'      => 'NOT-OK',
                              'message'     => 'status affiliate not active',
                              'code'        => "GEN-UNAUTHORIZED",
                              'http_code'   => 401
                            ]],401);
      }

      return response()->json(['success'=>[
                              'method'      => 'download',
                              'status'      => 'OK',
                              'affiliate'   => $affiliate,
                              'message'     => 'well done dude',
                              'code'        => "GEN-UNAUTHORIZED",
                              'http_code'   => 201
                            ]],200);
    }
}
