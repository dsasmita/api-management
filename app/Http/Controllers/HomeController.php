<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use Validator;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function profile()
    {
        return view('profile');
    }

    public function profilePost(Request $request){
        $validator = Validator::make($request->input(), [
           'name'           => 'required',
           'email'          => 'required|email',
           'password'       => 'required_if:change_password,checked|min:6|confirmed',
        ]);

        if (!$validator->fails()) {
            $user           = Auth::user();
            $user->name     = $request->input('name');
            $user->email    = $request->input('email');

            if($request->input('change_password') == 'checked'){
                $user->password = bcrypt($request->input('password'));
            }
            $user->save();
            return redirect()->back()
                        ->with([
                            'notif-success' => 'Update Profile Success'
                        ]);
        }else{
            return redirect()->back()
                    ->withErrors($validator)->withInput();
        }
    }
}
