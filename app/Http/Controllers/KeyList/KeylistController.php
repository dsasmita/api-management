<?php

namespace App\Http\Controllers\KeyList;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

use App\Model\Affiliate;
use App\Model\ApiKeys;

class KeylistController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->input('deleted') == 1){
            $arr_data['view_active']    = 'deleted';
            $arr_data['title']          = 'Deleted API Key';
            $aff = Affiliate::with('apiKey')
                            ->orderBy('created_at','desc')
                            ->onlyTrashed()->paginate($this->limit);
        }else{
            $arr_data['view_active']    = 'active';
            $arr_data['title']          = 'List API Key';
            $aff = Affiliate::with('apiKey')
                                ->orderBy('created_at','desc')
                                ->paginate($this->limit);
        }
        $arr_data['list']   = $aff;
        $arr_data['i']      = 1;
        $arr_data['offset'] = 1;

        $page = $request->input('page',1);
        if($page == 1){
            $arr_data['offset'] = 0;
        }else{
            $arr_data['offset'] = ($page-1)*$this->limit;
        }
        $arr_data['status_option'] = Affiliate::$status;
        return view('keyList.index',$arr_data);
    }

    public function add(){
        $arr_data = [];
        return view('keyList.add',$arr_data);
    }

    public function addPost(Request $request){
        $input['name']          = strtoupper($request->input('name'));
        $input['referral_code'] = strtoupper($request->input('referral_code'));

        $validator = Validator::make($input, [
           'name'           => 'required|unique:affiliate,name',
           'referral_code'  => 'required|unique:affiliate,referral_code',
        ]);
        if (!$validator->fails()) {

            $aff                = new Affiliate();
            $aff->name          = $input['name'];
            $aff->referral_code = $input['referral_code'];
            $aff->status        = 'not-active';
            $aff->save();

            $apiKey             = new \Chrisbjr\ApiGuard\Models\ApiKey;
            $apiKey->key        = $apiKey->generateKey();
            $apiKey->user_id    = $aff->id;
            $apiKey->level      = 10;
            $apiKey->ignore_limits = true;

            $apiKey->save();

            return redirect(route('backend.keylist'))->with([
                'notif-success' => 'Add new API key success.'
            ]);
        } else {
            return redirect()->back()
                    ->withErrors($validator)->withInput();
        }
    }

    public function update($id = null){
        $affiliate = Affiliate::with('apiKey')->find($id);

        if(!$affiliate){
            return redirect(route('backend.keylist'))->with([
                'notif-error' => 'API key not found.'
            ]);
        }
        $arr_data                               = [];
        $arr_data['detail']['id']               = $id;
        $arr_data['detail']['status']           = $affiliate->status;
        $arr_data['detail']['name']             = $affiliate['name'];
        $arr_data['detail']['referral_code']    = $affiliate['referral_code'];
        
        if(old('name') != '')
            $arr_data['detail']['name'] = old('name');

        if(old('referral_code') != '')
            $arr_data['detail']['referral_code'] = old('referral_code');

        if($affiliate->apiKey()->first()){
            $arr_data['apiKey'] = $affiliate->apiKey()->first();
        }

        $arr_data['status_option'] = Affiliate::$status;
        
        return view('keyList.update',$arr_data);
    }

    public function updatePost($id = null, Request $request){
        $affiliate = Affiliate::with('apiKey')->find($id);

        if(!$affiliate){
            return redirect(route('backend.keylist'))->with([
                'notif-error' => 'API key not found.'
            ]);
        }
        $input                  = [];
        $input['name']          = strtoupper($request->input('name'));
        $input['referral_code'] = strtoupper($request->input('referral_code'));

        $rules                  = [];
        $rules['name']          = 'required';
        $rules['referral_code'] = 'required';

        if($input['name'] != $affiliate['name']){
            $rules['name']      = 'required|unique:affiliate,name';
        }

        if($input['referral_code'] != $affiliate['referral_code']){
            $rules['referral_code'] = 'required|unique:affiliate,referral_code';
        }

        $validator = Validator::make($input,$rules);
        if (!$validator->fails()) {
            $affiliate->name            = $input['name'];
            $affiliate->referral_code   = $input['referral_code'];
            $affiliate->status          = $request->input('status');
            $affiliate->save();

            if(!$affiliate->apiKey()->first()){
                $apiKey             = new \Chrisbjr\ApiGuard\Models\ApiKey;
                $apiKey->key        = $apiKey->generateKey();
                $apiKey->user_id    = $affiliate->id;
                $apiKey->level      = 10;
                $apiKey->ignore_limits = true;
                $apiKey->save();
            }

            return redirect(route('backend.keylist'))->with([
                'notif-success' => 'Update API key success.'
            ]);
        }else{
            return redirect()->back()
                ->withErrors($validator)->withInput();
        }
    }

    public function delete($id = null){
        $affiliate = Affiliate::with('apiKey')->find($id);

        if(!$affiliate){
            return redirect(route('backend.keylist'))->with([
                'notif-error' => 'API key not found.'
            ]);
        }
        if($affiliate->apiKey()->first()){
            $apiKey = $affiliate->apiKey()->first();
            $apiKey->delete();
        }
        $affiliate->delete();

        return redirect(route('backend.keylist'))->with([
                        'notif-success' => 'Delete API key success.'
                    ]);
    }

    public function restore($id = null){
        $affiliate = Affiliate::with('apiKey')->onlyTrashed()->find($id);

        if(!$affiliate){
            return redirect(route('backend.keylist'))->with([
                'notif-error' => 'API key not found.'
            ]);
        }
        $affiliate->status = 'not-active';
        $affiliate->save();
        if($affiliate->apiKey()->onlyTrashed()->first()){
            $apiKey = $affiliate->apiKey()->onlyTrashed()->first();
            $apiKey->restore();
        }
        $affiliate->restore();

        return redirect(route('backend.keylist'))->with([
                        'notif-success' => 'Restore API key success.'
                    ]);
    }
}
