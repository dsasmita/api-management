<?php

namespace App\Http\Controllers\KeyLogs;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Model\ApiLogs;

class KeyLogsController extends Controller
{
    public function __construct()
    {
    }

    public function index(Request $request)
    {
    	$aff = ApiLogs::with(['apiKeys'])
                            ->orderBy('created_at','desc')
                            ->paginate($this->limit);

        $arr_data['list']   = $aff;
        $arr_data['offset'] = 1;

        $page = $request->input('page',1);
        if($page == 1){
            $offset = 0;
        }else{
            $offset = ($page-1)*$this->limit;
        }

        $arr_data['i']      = 1+$offset;

        return view('keyLogs.index',$arr_data);
    }
}
