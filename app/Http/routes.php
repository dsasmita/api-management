<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	if (Auth::guest()){
    	return view('welcome');
	}else{
		return redirect(route('backend.home'));
	}
});

//Auth route
Route::get('login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@showLoginForm']);
Route::post('login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@login']);
Route::get('logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@logout']);

/*
// Registration Routes...
Route::get('register', ['as' => 'auth.register', 'uses' => 'Auth\AuthController@showRegistrationForm']);
Route::post('register', ['as' => 'auth.register', 'uses' => 'Auth\AuthController@register']);

// Password Reset Routes...
Route::get('password/reset/{token?}', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@showResetForm']);
Route::post('password/email', ['as' => 'auth.password.email', 'uses' => 'Auth\PasswordController@sendResetLinkEmail']);
Route::post('password/reset', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@reset']);
*/

//auth route

Route::group(['middleware' => 'auth'], function () {
	Route::get('/home', ['as' => 'backend.home', 'uses' => 'HomeController@index']);
	
	Route::get('/profile', ['as' => 'backend.profile', 'uses' => 'HomeController@profile']);
	Route::post('/profile', ['as' => 'backend.profile.post', 'uses' => 'HomeController@profilePost']);

	Route::group(['namespace' => 'KeyList','prefix' => 'key-list'], function(){
		Route::get('/', ['as' => 'backend.keylist', 'uses' => 'KeylistController@index']);
		
		Route::get('/add', ['as' => 'backend.keylist.add', 'uses' => 'KeylistController@add']);
		Route::post('/add', ['as' => 'backend.keylist.add.post', 'uses' => 'KeylistController@addPost']);

		Route::get('/update/{id}', ['as' => 'backend.keylist.update', 'uses' => 'KeylistController@update'])->where(['id' => '[0-9]+']);
		Route::post('/update/{id}', ['as' => 'backend.keylist.update.post', 'uses' => 'KeylistController@updatePost'])->where(['id' => '[0-9]+']);
		
		Route::get('/delete/{id}', ['as' => 'backend.keylist.delete', 'uses' => 'KeylistController@delete'])->where(['id' => '[0-9]+']);
		Route::get('/restore/{id}', ['as' => 'backend.keylist.restore', 'uses' => 'KeylistController@restore'])->where(['id' => '[0-9]+']);
	});

	Route::group(['namespace' => 'KeyLogs','prefix' => 'key-logs'], function(){
		Route::get('/', ['as' => 'backend.keylogs', 'uses' => 'KeyLogsController@index']);

	});

});


Route::group(['namespace' => 'Api','prefix' => 'api'], function(){
	Route::get('/affiliate', ['as' => 'api.affiliate', 'uses' => 'AffiliateController@form']);
	Route::post('/affiliate', ['as' => 'api.affiliate.post', 'uses' => 'AffiliateController@form']);

	Route::get('/affiliate/pdf-download', ['as' => 'api.affiliate.download', 'uses' => 'AffiliateController@downloadPdf']);
	Route::post('/affiliate/pdf-download', ['as' => 'api.affiliate.download.post', 'uses' => 'AffiliateController@downloadPdf']);
});