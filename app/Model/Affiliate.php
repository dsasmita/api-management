<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Affiliate extends Model
{
	use SoftDeletes;
    protected $table 		= 'affiliate';
    public static $status   = [
    					'active' 		=> ['Active','Status Active'],
    					'not-active' 	=> ['Not Active','Status Not Active'],
    				];
    protected $dates = ['deleted_at'];

    public function apiKey()
	{
	    return $this->belongsTo('App\Model\ApiKeys', 'id','user_id');
	}
}
