<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApiKeys extends Model
{
	use SoftDeletes;
    protected $table = 'api_keys';
    protected $dates = ['deleted_at'];

    public function apiLogs()
	{
	    return $this->belongsTo('App\Model\ApiLogs', 'id','api_key_id');
	}

	public function affiliate()
	{
	    return $this->belongsTo('App\Model\Affiliate', 'user_id','id');
	}
}
