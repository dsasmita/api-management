<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ApiLogs extends Model
{
    protected $table = 'api_logs';

    public function apiKeys()
	{
	    return $this->belongsTo('App\Model\ApiKeys', 'api_key_id','id');
	}
}
