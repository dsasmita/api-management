@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    add API Key
                </div>
                <div class="panel-body">
                {!! Form::open(array('url' => route('backend.keylist.add.post'),'class'=>'form-horizontal')) !!}
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-6">
                            <input type="text" value="{{ old('name')}}" class="form-control" id="name" name="name" placeholder="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="referral_code" class="col-sm-2 control-label">Referral Code</label>
                        <div class="col-sm-6">
                            <input type="text" value="{{ old('referral_code') }}" class="form-control" name="referral_code" id="referral_code" placeholder="Referral Code">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection