@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{$title}}
                    <div class="pull-right">
                        @if($view_active == 'deleted')
                            <a href="{{ route('backend.keylist') }}" class="btn btn-xs btn-warning"><i class="fa fa-list"></i> Show Used</a>
                        @else
                            <a href="{{ route('backend.keylist') }}?deleted=1" class="btn btn-xs btn-warning"><i class="fa fa-refresh"></i> Show Deleted</a>
                        @endif
                        <a href="{{ route('backend.keylist.add') }}" class="btn btn-xs btn-primary"><i class="fa fa-plus"></i> Add New</a>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><center>#</center></th>
                                    <th><center>Name</center></th>
                                    <th><center>Referral Code</center></th>
                                    <th><center>API Key</center></th>
                                    <th><center>Status</center></th>
                                    <th><center>Action</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($list as $data)
                                <tr>
                                    <th scope="row">{{ $i++ }}</th>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->referral_code }}</td>
                                    <td>
                                        @if($data->apiKey()->withTrashed()->first())
                                            <input class="form-control" value="{{$data->apiKey()->withTrashed()->first()->key}}">
                                        @else
                                            -not set-
                                        @endif
                                    </td>
                                    <td>
                                        <center>
                                            @if($view_active == 'deleted')
                                                <span class="label label-danger">Deleted</span>
                                            @else
                                                @if($data->status != '')
                                                    <span class="label label-{{$data->status == 'active' ? 'info' : 'warning'}}">{{@$status_option[$data->status][1]}}</span>
                                                @else
                                                    <span class="label label-default">-not set-</span>
                                                @endif
                                            @endif
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <div class="btn-group" role="group" aria-label="Default button group"> 
                                                @if($view_active == 'active')
                                                    <a href="{{ route('backend.keylist.update',$data->id) }}" class="btn btn-default btn-info"><i class="fa fa-edit"></i></a>
                                                    <a href="javascript:void(0)" data-url="{{ route('backend.keylist.delete',$data->id) }}" class="btn btn-default btn-warning btn-delete"><i class="fa fa-trash"></i></a> 
                                                @else
                                                    <a href="javascript:void(0)" data-url="{{ route('backend.keylist.restore',$data->id) }}" class="btn btn-waring btn-warning btn-restore"><i class="fa fa-refresh"></i></a> 
                                                @endif
                                            </div>
                                        </center>
                                    </td>
                                </tr>
                                @endforeach
                                @if(count($list) == 0 )
                                    <tr>
                                       <td colspan="6"><center><em>-data empty-</em></center></td> 
                                    </tr>
                                @else
                                    <tr class="">
                                       <td colspan="6"><em>{{$list->count().' from '.$list->total().' data'}}</em></td> 
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <nav>
                            <center>
                                {{$list->render()}}
                            </center>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function() {
        $('.btn-delete').click(function(e){
            e.preventDefault();
            var url = $(this).data('url');
            var conf = confirm('Delete this API Key?');
            if(conf){
                window.location.replace(url);
            }
        });
        $('.btn-restore').click(function(e){
            e.preventDefault();
            var url = $(this).data('url');
            var conf = confirm('Restore this API Key?');
            if(conf){
                window.location.replace(url);
            }
        });
    });
</script>
@endsection