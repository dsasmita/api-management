@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    update API Key
                </div>
                <div class="panel-body">
                {!! Form::open(array('url' => route('backend.keylist.update.post',$detail['id']),'class'=>'form-horizontal')) !!}
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-6">
                            <input type="text" value="{{ $detail['name'] }}" class="form-control" id="name" name="name" placeholder="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="referral_code" class="col-sm-2 control-label">Referral Code</label>
                        <div class="col-sm-6">
                            <input type="text" value="{{ $detail['referral_code'] }}" class="form-control" name="referral_code" id="referral_code" placeholder="Referral Code">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="status" class="col-sm-2 control-label">Status</label>
                        <div class="col-sm-3">
                            <select class="form-control" name="status" id="status">
                                @foreach($status_option as $key=>$stat)
                                    <option {{$key == $detail['status'] ? 'selected' : ''}} value="{{$key}}">{{ $stat['0'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="key" class="col-sm-2 control-label">API Key</label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <input type="text" id="key" name="key" disabled="" class="form-control" value="{{ @$apiKey['key'] }}" placeholder="API Key">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="btn-copy" type="button"><i class="fa fa-copy"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function() {
        $('#btn-copy').click(function(e){
            e.preventDefault();
            var key = $('#key').val();
            copyToClipboard(key);
            if(key == ''){
                alert('Update First to get API Key');
            }else{
                alert('Key Success Copied');
            }
        });

        function copyToClipboard(element) {
            var $temp = $("<input>")
            $("body").append($temp);
            $temp.val(element).select();
            document.execCommand("copy");
            $temp.remove();
        }
    });
</script>
@endsection