@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    API Logs
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><center>#</center></th>
                                    <th><center>Name</center></th>
                                    <th><center>IP Add - Method - Route</center></th>
                                    <th><center>Time</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($list as $data)
                                <tr>
                                    <th scope="row"><center>{{ $i++ }}</center></th>
                                    <td>
                                        <?php $apiKey = $data->apiKeys()->withTrashed()->first() ?>  
                                        @if($apiKey)
                                            <?php $affiliate = \App\Model\Affiliate::withTrashed()->find($apiKey->user_id); ?>
                                            @if(!$affiliate->trashed())
                                                <a href="{{ route('backend.keylist.update',$affiliate->id) }}">{{$affiliate->name}} - {{$affiliate->referral_code}}</a><br>
                                                <small>{{str_limit($apiKey->key,12)}}</small>
                                            @else
                                                <a href="{{ route('backend.keylist') }}?deleted=1">-status deleted-</a><br>
                                                {{str_limit($apiKey->key,12)}}
                                            @endif
                                        @else
                                            -missing link-
                                        @endif
                                    </td>
                                    <td>
                                        {{$data->ip_address}} - {{$data->method}}<br>
                                        <small>{{$data->route}}</small>
                                    </td>
                                    <td><center>{{ date('Md Y H:i:s',strtotime($data->created_at)) }}</center></td>
                                </tr>
                                @endforeach
                                @if(count($list) == 0 )
                                    <tr>
                                       <td colspan="6"><center><em>-data empty-</em></center></td> 
                                    </tr>
                                @else
                                    <tr class="">
                                       <td colspan="6"><em>{{$list->count().' from '.$list->total().' data'}}</em></td> 
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <nav>
                            <center>
                                {{$list->render()}}
                            </center>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function() {
    });
</script>
@endsection