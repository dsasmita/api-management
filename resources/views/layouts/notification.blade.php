<div class="container">
	<div class="row">
        <div class="col-md-10 col-md-offset-1">
			@if(Session::get('notif-success'))
			<div class="alert alert-success alert-dismissable">
			    <i class="fa fa-check"></i>
			    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			    <b>Alert!</b> {!!Session::get('notif-success')!!}
			</div>
			@endif
			@if(Session::get('notif-error'))
			<div class="alert alert-danger alert-dismissable">
			    <i class="fa fa-ban"></i>
			    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			    <b>Alert!</b> {!!Session::get('notif-error')!!}
			</div>
			@endif

			@foreach ($errors->all() as $message)
			<div class="alert alert-danger alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong>Warning!</strong> {{$message}}
			</div>
			@endforeach
		</div>
	</div>
</div>