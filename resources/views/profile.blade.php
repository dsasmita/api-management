@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Profile</div>

                <div class="panel-body">
                    {!! Form::open(array('url' => route('backend.profile.post'),'class'=>'form-horizontal')) !!}
	                    <div class="form-group">
	                        <label for="name" class="col-sm-2 control-label">Name</label>
	                        <div class="col-sm-6">
	                            <input type="text" value="{{Auth::user()->name}}" class="form-control" id="name" name="name" placeholder="name">
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label for="email" class="col-sm-2 control-label">Email</label>
	                        <div class="col-sm-6">
	                            <input type="text" value="{{Auth::user()->email}}" class="form-control" name="email" id="email" placeholder="Email">
	                        </div>
	                    </div>
	                    <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" {{ old('change_password') == 'checked' ? 'checked' : '' }} id="change_password" value="checked" name="change_password"> Change password
                                    </label>
                                </div>
                            </div>
                        </div>
	                    <div id="form-password" class="{{ old('change_password') == 'checked' ? '' : 'hidden' }}">
		                    <legend>Change Password </legend>
		                    <div class="form-group">
		                        <label for="password" class="col-sm-2 control-label">Password</label>
		                        <div class="col-sm-6">
		                            <input type="password" value="" class="form-control" name="password" id="password" placeholder="password">
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label for="password_confirmation" class="col-sm-2 control-label">Verifikasi</label>
		                        <div class="col-sm-6">
		                            <input type="password" value="" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="verifikasi password">
		                        </div>
		                    </div>
	                    </div>
	                    <div class="form-group">
	                        <div class="col-sm-offset-2 col-sm-10">
	                            <button type="submit" class="btn btn-primary">Update</button>
	                        </div>
	                    </div>
	                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function() {
        $('#change_password').click(function() {
		    if(!$('#change_password').is(':checked')){
		    	$('#form-password').addClass('hidden');
		    }else{
		    	$('#form-password').removeClass('hidden');
		    }
		});
    });
</script>
@endsection